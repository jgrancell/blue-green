# Blue-Green

This project is a proof of concept to show highly-available, zero downtime blue-green
deployments using Kubernetes and GitOps.

![blue-green deployment architecture](https://imgur.com/8TBpzT3.png)

## Architecture and Explanation

### Application
The application, `th3-server.py`, is built into a container using Podman and Gitlab's CI
pipeline. It can be accessed manually through `docker pull registry.gitlab.com/jgrancell/blue-green:latest`
or `podman pull registry.gitlab.com/jgrancell/blue-green:latest`.

### CI Pipeline
The CI pipeline is run through Gitlab CI. This pipeline is defined in the `gitlab-ci.yaml` file,
and executed by CI runners.

Currently, this pipeline:
- Runs yamllint to look for syntax issues across the entire codebase.
- Runs `kustomize build deploy/overlays/production` to test the Application's GitOps code's ability to deploy.
- Runs buildah to build and save the Python application image

### GitOps
This repository, specifically the `deploy/` directory, is watched by ArgoCD. On updates, ArgoCD will reconcile any
changes in the live cluster.

This code includes a `production` overlay, that the Kubernetes cluster uses as its entrypoint. It then has
three branching paths through the code:
- Active -- the Ingress and Service that supports the active entrypoint for the Application
- Blue -- the blue environment
- Green -- the green environment

## How To Work The Updates

The general update process for this application is:

1. A developer updates whatever python code (such as the version number) in `src/` and commits the changes.
2. A developer should also update the blue or green environment they wish to roll out the new code to to use the new proposed tagged version.
3. Once any changes have passed a lesser QA/Acceptance environment (which would run off non-existant `overlay/{environment}` branches), the developer should tag a release with the proposed tagged version.
4. Once the release is tagged, it will automatically build the new image.
5. For this example, a manual update is then done to the ArgoCD configuration on the Kubernetes cluster to swap it to the new tagged release's ArgoCD code. In a real environment this would be done as an additional CI pipline step through either a manual or automatic trigger.
6. ArgoCD will then rollout the new application containers to the chosen environment, and that environment will become the primary active environment.

Important notes:
* In a regular production environment the ArgoCD application code would be in a separate repo from the application's source code.
    * This lets you tag separate ArgoCD releases without needing to rollout new image versions.
    * For the sake of this exercise, that has been omitted.
* Once the list above is done you'd want to upgrade the inactive environment to match the current environment, when you are sure that
the new environment is stable and a revert won't be needed. That is omitted from this example.
* All app deployments have a readinessProbe, so if for some reason the new environment doesn't come up it'll stay on its old version.

## Test Logs
Logs for the actual cutover test can be found in `test/runner.log`, which was run using a custom Go app that I wrote for this. It is approximately 5000 lines, corresponding with a 5000 request test. It polls a specific endpoint of the application (`/version` in this case) approximately 5-10 times per second and returns either the parsed json response from the API or an error.

For my test case, `blue` was running version `0.0.2` and `green` was running `0.1.0`. Blue was the active, but it was moved over to green after the `v0.1.0` release was deployed.

The pertinent information extracted from the runner logs from the few seconds surrounding the cutover are are:
```
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:02 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.1.0
2021-03-04 15:23:03 [INFO] Found version 0.0.2
2021-03-04 15:23:03 [INFO] Found version 0.1.0
2021-03-04 15:23:03 [INFO] Found version 0.1.0
2021-03-04 15:23:03 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:04 [INFO] Found version 0.1.0
2021-03-04 15:23:05 [INFO] Found version 0.1.0
2021-03-04 15:23:05 [INFO] Found version 0.1.0
2021-03-04 15:23:05 [INFO] Found version 0.1.0
2021-03-04 15:23:05 [INFO] Found version 0.1.0
```

Pertinent logs from my test runner:
```
./blue-green/test ❯ SERVER_HOST=http://bg-app.joshgrancell.com SERVER_URI="/version" QUERY_COUNT=5000 ./runner
Completed 5000 total queries to the API in 6m7.540114069s seconds
  - Successes: 4995
  - Failures: 5
```

The five recorded failures were due to an undersized Loadbalancer being used for this test, and weren't related to the blue-green cutover. They can be seen in the full log regardless.

The endpoints are available at the following locations:
- [https://bg-app.joshgrancell.com/verson](https://bg-app.joshgrancell.com/version) - The active application [currently pointed at green]
- [https://green.bg-app.joshgrancell.com/version](https://green.bg-app.joshgrancell.com/version) - The dedicated green-only portion of the application, which a developer could use to test before making the active app swap.
- [https://blue.bg-app.joshgrancell.com/version](https://blue.bg-app.joshgrancell.com/version) - The dedicated blue-only portion of the application, which a developer could use to validate regressions/view the previous version/etc.
