package main

import (
  "encoding/json"
  "fmt"
  "io/ioutil"
  "net/http"
  "net/url"
  "os"
  "strconv"
  "time"
)

// This function logs our connections to a file named runner.log in the current directory
func log(message string, error bool) {
  var prefix string
  if error {
    prefix = "[ERROR]"
  } else {
    prefix = "[INFO]"
  }

  cur := time.Now()
  timestamp := cur.Format("2006-01-02 15:04:05")
  f, _ := os.OpenFile("./runner.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY,0640)
  defer f.Close()

  _, err := f.WriteString(timestamp + " " + prefix + " " + message + "\n")
  if err != nil {
    panic(err)
  }
  f.Sync()
}

// Wrapper function for info logging
func info(message string) {
  log(message, false)
}

// Wrapper function for fatal error logging
func fatal(message string) {
  log(message, true)
}

func main() {
  start := time.Now()

  var host string
  var uri string
  var count int

  // Setting sane defaults for testing using local docker, overrideable with ENV values
  if _, ok := os.LookupEnv("SERVER_HOST"); ! ok {
    host = "http://127.0.0.1:8080"
  } else {
    host = os.Getenv("SERVER_HOST")
  }

  if _, ok := os.LookupEnv("SERVER_URI"); ! ok {
    uri = "/api/v1/translate?phrase=Lol"
  } else {
    uri = os.Getenv("SERVER_URI")
  }

  if _, ok := os.LookupEnv("QUERY_COUNT"); ! ok {
    count = 5
  } else {
    count, _ = strconv.Atoi(os.Getenv("QUERY_COUNT"))
  }

  // Building the HTTP request from our ENVs/defaults
  reqUrl, _ := url.Parse(host + uri)

  // Creating the HTTP request
  request := &http.Request {
    Method: "GET",
    URL: reqUrl,
    Header: map[string][]string {
      "Content-Type": {"application/json"},
      "User-Agent": {"Blue-Green App Status Fetcher"},
    },
    Body: nil,
  }

  // Setting an artificially low timeout here because this should be a very performant API
  //   - Requiring connection to function within 0.02 seconds
  var httpClient *http.Client
  httpClient = &http.Client{Timeout: 200 * time.Millisecond}

  iter := 0
  successCount := 0
  failureCount := 0
  for iter < count {
    iter += 1
    resp, err := httpClient.Do(request)
    if err != nil {
      fatal("Error executing HTTP call to " + uri + ": " + err.Error())
      failureCount += 1
    } else {
      bodyBytes, err := ioutil.ReadAll(resp.Body)
      if err != nil {
        fatal("Error reading HTTP response body from " + uri + ": " + err.Error())
        failureCount += 1
      } else {

        if resp.StatusCode != 200 {
          fatal("Received status code " + strconv.Itoa(resp.StatusCode))
          failureCount += 1
        } else {
          var th3 Version
          jsonErr := json.Unmarshal(bodyBytes, &th3)
          if jsonErr != nil {
            fatal("Error reading json response from API endpoint " + uri + ": " + err.Error())
            failureCount += 1
          } else {
            if len(th3.Version) >= 1 {
              info("Found version " + th3.Version)
            }
            successCount += 1
          }
        }
      }
    }
  }

  duration := time.Since(start)

  fmt.Println("Completed", count, "total queries to the API in", duration, "seconds")
  fmt.Println("  - Successes:", successCount)
  fmt.Println("  - Failures:", failureCount)
}
