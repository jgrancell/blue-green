package main

type Th3 struct {
	Errors      []string `json:"errors"`
	Phrase      string        `json:"phrase"`
	Translation string        `json:"translation"`
}

type Version struct {
	Errors      []string `json:"errors"`
	Version      string  `json:"version"`
}
